package com.springloltask.demo.repository;

import com.springloltask.demo.entities.LolChampion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface LolChampionRepository extends JpaRepository<Long, LolChampion> {
}
